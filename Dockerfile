FROM ubuntu:latest 

MAINTAINER Elijah Shope <eshope7489@gmail.com>

RUN apt-get update
RUN apt upgrade -y
RUN apt install git -y
RUN git clone https://tntuser66@bitbucket.org/tntuser66/minecraft.git /home/minecraft/
## Extract the archive
#RUN tar -xzvf home/minecraft/jdk-*.tar.gz
# mk the jvm dir
#RUN mkdir -p /usr/lib/jvm
# move the server jre
#RUN mv home/minecraft/jdk-11.* /usr/lib/jvm/oracle_jdk11
#
#RUN update-alternatives --install /usr/bin/java java /usr/lib/jvm/oracle_jdk11/bin/java 2000
#RUN update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/oracle_jdk11/bin/javac 2000
#
#RUN echo "export J2SDKDIR=/usr/lib/jvm/oracle_jdk11
#RUN export J2REDIR=/usr/lib/jvm/oracle_jdk11/
#RUN export PATH=$PATH:/usr/lib/jvm/oracle_jdk11/bin
#RUN export JAVA_HOME=/usr/lib/jvm/oracle_jdk11" | sudo tee -a /etc/profile.d/oraclejdk.sh
RUN javainstall.sh
ENTRYPOINT home/minecraft/ServerStart.sh -D FOREGROUND
VOLUME /home/tntuser55/Games/serverfiles
EXPOSE 25565

