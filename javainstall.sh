cd /home/minecraft
# Extract the archive
tar -xzvf jdk-*.tar.gz
# clean up the tar
rm -fr jdk-*.tar.gz
# mk the jvm dir
mkdir -p /usr/lib/jvm
# move the server jre
mv jdk-11.* /usr/lib/jvm/oracle_jdk11

update-alternatives --install /usr/bin/java java /usr/lib/jvm/oracle_jdk11/bin/java 2000
update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/oracle_jdk11/bin/javac 2000

echo "export J2SDKDIR=/usr/lib/jvm/oracle_jdk11
export J2REDIR=/usr/lib/jvm/oracle_jdk11/
export PATH=$PATH:/usr/lib/jvm/oracle_jdk11/bin
export JAVA_HOME=/usr/lib/jvm/oracle_jdk11" |  tee -a /etc/profile.d/oraclejdk.sh
